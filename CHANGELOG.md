# coreplus/main

## 0.1.1

### Patch

- fix PRINT_VIEW_CLASS in configs [#1](https://gitlab.com/zeroplus/django/django-coreplus/-/issues/1)
- add antispam to coreplus.spam [amel](https://gitlab.com/amelrnt) and [wilsen](https://gitlab.com/wilsen.widjaja1)
- fix alpinejs and htmx on coreplus.admin [sasri](https://gitlab.com/sasriawesome)
